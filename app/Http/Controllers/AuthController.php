<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        return view('halaman.form');
    }
    public function kirim(Request $request)
    {
        //dd($request->all());
        $fname = $request->fname;
        $lname = $request->lname;

        return view('halaman.kirim', compact('fname', 'lname'));
    }
}
