@extends('layout.master')
@section('judul')
Tambah Cast
@endsection
    
@section('content')
  
<form action="/cast" method="POST">
          @csrf
          <div class="form-group">
              <label>NAMA</label>
              <input type="text" class="form-control" name="nama" placeholder="Masukkan Title">
              @error('nama')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
              @enderror
          </div>
          <div class="form-group">
            <label>UMUR</label>
            <input type="text" class="form-control" name="umur" placeholder="Masukkan Title">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
          <div class="form-group">
              <label>BIO</label>
              <textarea name="bio" class="form-control" cols="30" rows="10"></textarea>
              
              @error('umur')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
              @enderror
          </div>
          <button type="submit" class="btn btn-primary">Tambah</button>
      </form>      





@endsection