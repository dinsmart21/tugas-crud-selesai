<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/form', 'AuthController@form');
Route::post('/kirim', 'AuthController@kirim');
route::get('/data-table', function () {
    return view('table.data-table');
});
route::get('/table', function () {
    return view('table.table');
});


//CRUD peran
route::get('/cast/create', 'castcontroller@create');
route::post('/cast', 'castcontroller@store');
route::get('/cast', 'castcontroller@index');
route::get('/cast/{cast_id}', 'castcontroller@show');
route::get('/cast/{cast_id}/edit', 'castcontroller@edit');
route::put('/cast/{cast_id}', 'castcontroller@update');
route::delete('/cast/{cast_id}', 'castcontroller@destroy');
